<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  public function cities()
  {
      return $this->hasMany('App\City');
  }
  public function openstatuses()
  {
      return $this->hasMany('App\OpenStatus');
  }
  public function visas()
  {
      return $this->hasMany('App\Visa');
  }
}
