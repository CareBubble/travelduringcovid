<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenStatus extends Model
{
    public function country()
    {
      return $this->belongsTo('App\Country', 'country_id');
    }
}
