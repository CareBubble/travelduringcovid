<?php

namespace App\Http\Controllers;

use App\Country;
use App\Expense;
use App\Flight;
use Illuminate\Http\Request;

class CountryController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $countries = Country::where('id', '!=', 0)->with('cities', 'openstatuses', 'visas')->get();

      foreach ($countries as $key => $country) {
          //dd($country[0]);
          $cities = $country->cities;
          foreach ($cities as $key => $city) {
              $expenses = Expense::where('city_id', $city->id)->get();
              $country->cities[$key]->expenses = $expenses;

              $flights = Flight::where('city_id', $city->id)->get();
              //dd($flights);
              $country->cities[$key]->flights = $flights;
          }
      }
      return view('home', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('countryCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = new  \App\Country();
        $country->title = $request->country_title;
        $country->continent = $request->country_continent;
        $country->description = $request->country_description;
        $country->save();
        $city = new  \App\City();
        $city->country_id = $country->id;
        $city->title = $request->city_title;
        $city->description = $request->city_description;
        $city->saftey = $request->city_saftey;
        $city->save();
        $expense = new \App\Expense();
        $expense->city_id = $city->id;
        $expense->food = $request->expense_food;
        $expense->travel = $request->expense_transport;
        $expense->accomodations = $request->expense_accomodations;
        $expense->save();
        $flight = new \App\Flight();
        $flight->city_id = $city->id;
        $flight->cost = $request->expense_flight;
        $flight->save();
        $visa = new \App\Visa();
        $visa->country_id = $country->id;
        $visa->required_for_one = $request->visas_required_for_one;
        $visa->required_for_two = $request->visas_required_for_two;
        $visa->length_of_stay = $request->visas_length_of_stay;
        $visa->save();
        $status = new \App\OpenStatus();
        $status->country_id = $country->id;
        $status->status = $request->open_status_status;
        $status->quarantine = $request->open_status_quarantine;
        $status->quarantine_costs = $request->open_status_quarantine_costs;
        $status->notes = "";
        $status->save();
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::where('id', $id)->with('cities', 'openstatuses', 'visas')->get();
        $country = $country[0];
        //dd($country);
        $cities = $country->cities;
        foreach ($cities as $key => $city) {
            $expenses = Expense::where('city_id', $city->id)->get();
            $country->cities[$key]->expenses = $expenses;
            $flights = Flight::where('city_id', $city->id)->get();
            //dd($flights);
            $country->cities[$key]->flights = $flights;
        }
        //return($country);
        return view('showCountry', compact('country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }
}
