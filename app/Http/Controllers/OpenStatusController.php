<?php

namespace App\Http\Controllers;

use App\OpenStatus;
use Illuminate\Http\Request;

class OpenStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OpenStatus  $openStatus
     * @return \Illuminate\Http\Response
     */
    public function show(OpenStatus $openStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OpenStatus  $openStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(OpenStatus $openStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OpenStatus  $openStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpenStatus $openStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OpenStatus  $openStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpenStatus $openStatus)
    {
        //
    }
}
