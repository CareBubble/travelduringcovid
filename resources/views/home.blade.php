@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="">
      <div>
        <div class="">
          <div class="table100 ver1 m-b-110">
            <div class="table100-head table-responsive">
              <table class="table">
                <thead>
                  <tr class="row100 head">
                    <th class="cell100 column1" style="width:10%">Country</th>
                    <th class="cell100 column2">Cities</th>
                    <th class="cell100 column3">Status</th>
                    <th class="cell100 column4">Visas</th>
                    <th class="cell100 column5" style="width:10%">Flights</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="table100-body js-pscroll">
              <table>
                <tbody>
                  <tr>
                      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                  </tr>
                  @foreach($countries as $country)
                      <tr class="row100 body">
                        <td class="cell100 column1" style="width:10%"><a href="/country/{{$country->id}}">{{$country->title}}</a></td>
                        <td class="cell100 column2">
                            @foreach($country->cities as $city)
                                {{$city->title}}<br>
                                Food: £{{$city->expenses[0]->food}}<br>
                                Travel: £{{$city->expenses[0]->travel}}<br>
                                Stay: £{{$city->expenses[0]->accomodations}}<br>
                            @endforeach
                        </td>
                        <td class="cell100 column3">
                            Status: {{$country->openstatuses[0]->status}}<br>
                            Qtn?: {{$country->openstatuses[0]->quarantine}}<br>
                            Qtn Cost: £{{$country->openstatuses[0]->quarantine_costs}}
                        </td>
                        <td class="cell100 column4">
                            Jay: {{$country->visas[0]->required_for_one}}<br>
                            Gyo: {{$country->visas[0]->required_for_two}}<br>
                            Length: {{$country->visas[0]->length_of_stay}}
                        </td>
                        <td class="cell100 column5" x`style="width:10%">£{{$city->flights[0]->cost}}</td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
