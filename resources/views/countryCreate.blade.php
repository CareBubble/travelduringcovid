@extends('layouts.app')

@section('content')
    <div class="row">
        <form action="/country/store" method="post" style="width:100%">
          @csrf
          <div class="col-md-12" style="background:#e9ecef; padding:30px;">
              <h2>Add a country</h2>
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="country_title" placeholder="brazil">
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="country_continent" placeholder="South America">
              <p>&nbsp;</p>
              <textarea class="form-control" style="min-height:200px;" name="country_description">Description</textarea>
          </div>
          <p>&nbsp;</p>
          <div class="col-md-12" style="background:#e9ecef; padding:30px;">
              <h2>Add at least one city</h2>
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="city_title" placeholder="Sao Paulo">
              <p>&nbsp;</p>
              <textarea class="form-control" style="min-height:200px;" name="city_description">Description</textarea>
              <p>&nbsp;</p>
              <textarea class="form-control" style="min-height:200px;" name="city_saftey">saftey</textarea>
          </div>
          <p>&nbsp;</p>
          <div class="col-md-12" style="background:#e9ecef; padding:30px;">
              <h2>Add Expenses</h2>
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="expense_food" placeholder="Food">
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="expense_transport" placeholder="Transport">
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="expense_accomodations" placeholder="Accomodations">
              <p>&nbsp;</p>
          </div>
          <p>&nbsp;</p>
          <div class="col-md-12" style="background:#e9ecef; padding:30px;">
              <h2>How much is a flight there?</h2>
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="expense_flight" placeholder="1000.00">
          </div>
          <p>&nbsp;</p>
          <div class="col-md-12" style="background:#e9ecef; padding:30px;">
              <h2>Do we need a visa?</h2>
              <p>&nbsp;</p>
              <select class="form-control" name="visas_required_for_one">
                  <option>Do I need a visa?</option>
                  <option>Yes</option>
                  <option>No</option>
              </select>
              <p>&nbsp;</p>
              <select class="form-control" name="visas_required_for_two">
                  <option>Does Gyo need a visa?</option>
                  <option>Yes</option>
                  <option>No</option>
              </select>
              <p>&nbsp;</p>
              <input type="text" class="form-control" name="visas_length_of_stay" placeholder="180 days">
              <p>&nbsp;</p>
          </div>
          <p>&nbsp;</p>
          <div class="col-md-12" style="background:#e9ecef; padding:30px;">
              <h2>What's the covid situation?</h2>
              <p>&nbsp;</p>
              <input type="text" class="form-control"  name="open_status_status" placeholder="Open (Test Required)">
              <p>&nbsp;</p>
              <select class="form-control" name="open_status_quarantine">
                  <option>Do you need to quarantine?</option>
                  <option>Yes</option>
                  <option>No</option>
              </select>
              <p>&nbsp;</p>
              <input type="text" class="form-control" name="open_status_quarantine_costs" placeholder="100.00">
              <p>&nbsp;</p>
          </div>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <button type="submit" class="btn btn-primary form-control">Add Country</button>
        </form>
    </div>
@endsection
