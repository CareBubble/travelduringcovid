@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="">
      <div>
        <div class="">
          <div class="table100 ver1 m-b-110">
            <div class="table100-head table-responsive">
              <table class="table">
                <thead>
                  <tr class="row100 head">
                      <th class="cell100 column1" style="width:100%" colspan="5"><center>{{$country->title}}</center></th>
                  </tr>
                  <tr class="row100 head">
                    <th class="cell100 column1" style="width:10%!important;">Continent</th>
                    <th class="cell100 column2" style="width:30%!important;">Description</th>
                    <th class="cell100 column3">Status</th>
                    <th class="cell100 column4">Visas</th>
                    <th class="cell100 column5">Flights</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="table100-body">
              <table>
                <tbody>
                  <tr>
                      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                  </tr>
                  <tr>
                      <td class="column1" style="width:10%!important;">{{$country->continent}}</td>
                      <td class="column2" style="width:30%!important;">{{$country->description}}</td>
                      <td class="column3">{{$country->openstatuses[0]->status}}</td>
                      <td class="column4">
                          Jay: {{$country->visas[0]->required_for_one}}<br>
                          Gyo: {{$country->visas[0]->required_for_two}}<br>
                          Length: {{$country->visas[0]->length_of_stay}}
                      </td>
                      <td class="column5">£{{$country->cities[0]->flights[0]->cost}}</td>
                  </tr>
              </table>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <table class="table">
                  <tr class="row100 head">
                    <th class="cell100 column1">City Name</th>
                    <th class="cell100 column2">Food</th>
                    <th class="cell100 column3">Travel</th>
                    <th class="cell100 column4">Accomodations</th>
                    <th class="cell100 column5">Saftey (opinion)</th>
                  </tr>
                  @foreach($country->cities as $key1 => $city)
                      <?php $expense = $city->expenses[0];?>
                      <tr class="row100 body">
                        <td class="cell100 column1"><a href="/country/{{$country->id}}">{{$city->title}}</a></td>
                        <td class="cell100 column1"><a href="/country/{{$country->id}}">£{{$expense->food}}/month</a></td>
                        <td class="cell100 column1"><a href="/country/{{$country->id}}">£{{$expense->travel}}/month</a></td>
                        <td class="cell100 column1"><a href="/country/{{$country->id}}">£{{$expense->accomodations}}/month</a></td>
                        <td class="cell100 column1" style="width:50%"><a href="/country/{{$country->id}}">{{$city->saftey}}</a></td>
                      </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
