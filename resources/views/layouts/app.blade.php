
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/jumbotron/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/jumbotron/jumbotron.css" rel="stylesheet">
    <link href="https://colorlib.com/etc/tb/Table_Fixed_Header/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://colorlib.com/etc/tb/Table_Fixed_Header/vendor/select2/select2.min.css" rel="stylesheet">
    <link href="https://colorlib.com/etc/tb/Table_Fixed_Header/css/util.css" rel="stylesheet">
    <link href="https://colorlib.com/etc/tb/Table_Fixed_Header/css/main.css" rel="stylesheet">
    <link href="https://colorlib.com/etc/tb/Table_Fixed_Header/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet">

    <style>
        .btn-primary {
          color: #fff;
          background-color: #6c7ae0;
          border-color: #6c7ae0;
        }
        .column1{
            width:auto;
            padding: 20px;
        }
        .table100-body{
          max-height: 100%;
        }
    </style>
  </head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="#">Add Country</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#">Add City</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#">Add Expenses</a>
          </li>h
          <li class="nav-item">
            <a class="nav-link " href="#">Add Visa</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#">Add Open</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#">Add Flights</a>
          </li>
        </ul>
        <span class="form-inline my-2 my-lg-0">
          <ul class="navbar-nav mr-auto">
            @guest
              <a href="/login"><button class="btn btn-primary btn-lg my-2 my-sm-0">Login</button</a></a><a href="/register"> <button class="btn btn-outline-primary btn-lg my-2 my-sm-0">Register</button</a></a>
            @else
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><button class="btn btn-primary btn-lg my-2 my-sm-0" type="submit">{{{ Auth::user()->name }}}</button></a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="#">My profile</a>
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                </div>
              </li>
            @endguest
          </ul>
        </span>
      </div>
    </nav>

    <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h1 class="display-3">Travel During Covid!</h1>
          <p>When you add a country, you'll be assked to add a city, the cost of living in that city, the visa situation and covid openess of the country and flight details on how to get there. You can add each individually above.</p>
          <p>&nbsp;</p>
          <p><a class="btn btn-primary btn-lg" href="/country/new" role="button">Add country &raquo;</a></p>
        </div>
      </div>

      <div class="container">
          @yield('content')
      </div> <!-- /container -->

    </main>

    <footer style="margin:20px;-">
        <Center><p>&copy; Company 2017-2018</p><center>
    </footer>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
  </body>
</html>
