<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'CountryController@index')->name('top');
Route::get('/home', 'CountryController@index')->name('home');
Route::get('/countries', 'CountryController@index')->name('country_index');
Route::get('/country/new', 'CountryController@create')->name('country_create');
Route::post('/country/store', 'CountryController@store')->name('country_store');
Route::get('/country/{id}', 'CountryController@show')->name('country_show');
